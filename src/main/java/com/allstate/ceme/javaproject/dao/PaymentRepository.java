package com.allstate.ceme.javaproject.dao;

import com.allstate.ceme.javaproject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class PaymentRepository implements PaymentDAO {

    @Autowired
    private MongoTemplate tpl;

    @Override
    public long rowCount() {
        Query query = new Query();
        return tpl.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));

        Payment payment = tpl.findOne(query,Payment.class);

        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));

        List<Payment> payments = new ArrayList<Payment>();
        payments = tpl.find(query, Payment.class, "payment");

        return payments;
    }

    @Override
    public List<Payment> returnAllPayments() {
        return tpl.findAll(Payment.class);
    }

    @Override
    public int save(Payment payment) {
        int nextAvailableId = 1;
        try{
            nextAvailableId = (this.getLastPayment().getId() + 1);
        }
        catch(Exception e) {
        //Log error
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(cal.getTime());
        Date today = cal.getTime();

        payment.setId(nextAvailableId);
        payment.setPaymentDate(today);

        tpl.insert(payment);
        return 0;
    }

    @Override
    public Payment getLastPayment(){
        Query query = new Query();
        List<Payment> payments = new ArrayList<Payment>();

        payments = tpl.find(query, Payment.class, "payment");
        payments.sort(new Comparator<Payment>() {
            @Override
            public int compare(Payment pay1, Payment pay2) {
                return pay1.getId() - pay2.getId();
            }
        });

        return(payments.get(payments.size()-1));
    }
}
