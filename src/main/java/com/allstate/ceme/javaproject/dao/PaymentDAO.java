package com.allstate.ceme.javaproject.dao;

import com.allstate.ceme.javaproject.entities.Payment;

import java.util.List;

public interface PaymentDAO {
    long rowCount();
    Payment findById(int id);
    List<Payment> returnAllPayments();
    List<Payment> findByType(String type);
    int save(Payment payment);
    Payment getLastPayment();
}
