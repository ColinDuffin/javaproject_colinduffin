package com.allstate.ceme.javaproject.rest;

import com.allstate.ceme.javaproject.entities.Payment;
import com.allstate.ceme.javaproject.services.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {
    @Autowired
    IPaymentService service;

    @RequestMapping(value="/status", method = RequestMethod.GET)
    public String status(){
        return "OK";
    }

    @RequestMapping(value="/rowcount", method = RequestMethod.GET)
    public long rowCount(){
        return service.rowCount();
    }

    @RequestMapping(value="/getallpayments", method = RequestMethod.GET)
    public List<Payment>  returnAllPayments(){
        return service.returnAllPayments();
    }

    @RequestMapping(value="/findbyid/{id}", method = RequestMethod.GET)
    public Payment findById(@PathVariable("id") int id){
        return service.findById(id);
    }

    @RequestMapping(value="/findbytype/{type}", method = RequestMethod.GET)
    public List<Payment> findByType(@PathVariable("type") String type){
        return service.findByType(type);
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public void save(@RequestBody Payment payment){
        service.save(payment);
    }

}
