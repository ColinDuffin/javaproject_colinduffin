package com.allstate.ceme.javaproject.services;

import com.allstate.ceme.javaproject.dao.PaymentDAO;
import com.allstate.ceme.javaproject.entities.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService implements  IPaymentService {

    @Autowired
    private PaymentDAO paymentDAO;
    private static Logger logger = LoggerFactory.getLogger(PaymentService.class);
    public PaymentService() {
    }

    @Override
    public long rowCount() {
        return paymentDAO.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if(id > 0){
            return paymentDAO.findById(id);}
        else{
            logger.error(MessageFormat.format("Payment Record of ID {0} does not exist.", id));
            return null;
        }
    }

    @Override
    public List<Payment> returnAllPayments() {
            //List<Payment> payments = new ArrayList<Payment>();
            //payments = paymentDAO.returnAllPayments();
            return paymentDAO.returnAllPayments();
       }

    @Override
    public List<Payment> findByType(String type) {
        if (type != null) {
            List<Payment> payments = new ArrayList<Payment>();
            payments = paymentDAO.findByType(type);
            return paymentDAO.findByType(type);
        } else {
            logger.error(MessageFormat.format("Payment Record of type {0} does not exist.", type));
            return null;
        }
    }

    @Override
    public int save(Payment payment) {
        int saveSuccess = paymentDAO.save(payment);
        int paymentId = payment.getId();

        if (saveSuccess == 0){
           logger.info(MessageFormat.format("Payment Record ID {0} added successfully.", paymentId));
           return 0;
        }
        else{
            logger.error(MessageFormat.format("Payment Record ID {0} was not added.", paymentId));
            return -1;
        }
    }

    @Override
    public Payment getLastPayment() {
        return paymentDAO.getLastPayment();
    }
}
