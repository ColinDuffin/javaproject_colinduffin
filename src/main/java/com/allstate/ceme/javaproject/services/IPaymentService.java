package com.allstate.ceme.javaproject.services;

import com.allstate.ceme.javaproject.entities.Payment;

import java.util.List;

public interface IPaymentService {
    long rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> returnAllPayments();
    int save(Payment payment);
    Payment getLastPayment();

}
