package com.allstate.ceme.javaproject.services;

import com.allstate.ceme.javaproject.dao.PaymentDAO;
import com.allstate.ceme.javaproject.entities.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentServiceTest {
    @Autowired
    IPaymentService paymentService;

    @Test
    public void savePayment(){
        int nextAvailableId = (paymentService.getLastPayment().getId() + 1);
        Date today = Calendar.getInstance().getTime();

        Payment payment = new Payment(nextAvailableId, today,"Monthly", 1234.00,100001);
        int returnValue = paymentService.save(payment);
        Assert.isTrue(returnValue == 0,"Return value 0");
    }

    @Test
    public void getPaymentById(){
        Payment payment = paymentService.findById(1);
        Assert.isTrue(payment.getId() == 1,"Payment returned by id");
    }

    @Test
    public void getPaymentByType(){
        List<Payment> payments = new ArrayList<Payment>();
        payments = paymentService.findByType("Monthly");
        Assert.isTrue(!payments.isEmpty(), "Payments list returned by type");
    }

    @Test
    public void getRowCount(){
        Assert.isTrue(paymentService.rowCount() > 0, "Return Row Count");
    }


}
