package com.allstate.ceme.javaproject.entities;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.Calendar;
import java.util.Date;

public class PaymentTest {
    @Test
    public void createPaymentObject(){
        Date today = Calendar.getInstance().getTime();
        Payment payment = new Payment(001, today,"Monthly", 1234.00,100001);
        Assert.isInstanceOf(Payment.class,payment);
    }

    @Test void paymentToString(){
        Date today = Calendar.getInstance().getTime();
        Payment payment = new Payment(001, today,"Monthly", 1234.00,100001);
        String contents = payment.toString();
        Assert.isTrue(contents.length() > 0, "object values to string");

    }
}
