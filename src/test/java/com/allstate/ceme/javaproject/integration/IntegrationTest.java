package com.allstate.ceme.javaproject.integration;

import com.allstate.ceme.javaproject.entities.Payment;
import com.allstate.ceme.javaproject.JavaprojectApplication;
import com.allstate.ceme.javaproject.rest.PaymentController;
import com.allstate.ceme.javaproject.services.PaymentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

    @LocalServerPort
    private  int port;

    @Autowired
    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void testServiceStatus(){
        Assert.isTrue(this.restTemplate
                            .getForObject("http://localhost:"+ port + "/api/status", String.class)
                            .contains("OK"), "Service returns OK status"
        );
    }

}
