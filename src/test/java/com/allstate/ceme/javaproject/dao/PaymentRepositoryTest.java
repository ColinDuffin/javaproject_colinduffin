package com.allstate.ceme.javaproject.dao;

import com.allstate.ceme.javaproject.entities.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentRepositoryTest {
    @Autowired
    PaymentDAO dao;

    @Test
    public void savePayment(){
        int nextAvailableId = (dao.getLastPayment().getId() + 1);
        Date today = Calendar.getInstance().getTime();
        Payment payment = new Payment(nextAvailableId, today,"Monthly", 1234.00,100004);
        int returnValue = dao.save(payment);
        Assert.isTrue(returnValue == 0,"Return value 0");
    }

    @Test
    public void getPaymentById(){
        Payment payment = dao.findById(001);
        Assert.isTrue(payment.getId() == 001,"Return payment id 001");
    }

    @Test
    public void getPaymentByType(){
        List<Payment> payments = new ArrayList<Payment>();
        payments = dao.findByType("Monthly");
        Assert.notNull(payments, "Payments is not null");
    }

    @Test
    public void getRowCount(){
        Long rowCount = dao.rowCount();
        Assert.isTrue(rowCount > 0 ,"Return DB Row Count");
    }


}
